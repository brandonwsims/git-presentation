#include <iostream>

using std::cout;
using std::endl;

int main() {
    cout << "Someone else was here" << endl;
    cout << "Someone else wrote this" << endl;
    cout << "This is the dev branch" << endl;
    return 0;
}